#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <limits>
#include <chrono>
#include <string>

class measures {
private:
    std::vector<double> stator_winding;
    std::vector<double> u_d;
    std::vector<double> pm;

public:
    measures(const std::string& file_path) {
        std::ifstream file(file_path);
        if (!file) {
            std::cerr << "Error opening file: " << file_path << std::endl;
            return;
        }


        std::string line;
        std::getline(file, line); 
        int row = 0;
        while (std::getline(file, line) && row < 500000) {
            std::istringstream iss(line);
            double stator, ud, p_m;
            for (int i = 0; i < 3; ++i) {
                std::getline(iss, line, ',');
                double value = std::stod(line);
                if (i == 0) stator = value;
                else if (i == 1) ud = value;
                else if (i == 2) p_m = value;
            }
            stator_winding.push_back(stator);
            u_d.push_back(ud);
            pm.push_back(p_m);
            row++;
        }
        file.close();

        sort_vector(stator_winding);
        sort_vector(u_d);
        sort_vector(pm);
    }

    void sort_vector(std::vector<double>& vec) {
        std::sort(vec.begin(), vec.end());
    }

    double get_min_value(const std::vector<double>& data) {
        auto start = std::chrono::high_resolution_clock::now();

        double min_val = data.empty() ? std::numeric_limits<double>::quiet_NaN() : data.front();

        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double, std::milli> elapsed = end - start;
        std::cout << "Dohvacanje najmanje vrijednosti trajalo je: " << elapsed.count() << " ms." << std::endl;

        return min_val;
    }

    double get_max_value(const std::vector<double>& data) {
        auto start = std::chrono::high_resolution_clock::now();

        double max_val = data.empty() ? std::numeric_limits<double>::quiet_NaN() : data.back();

        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double, std::milli> elapsed = end - start;
        std::cout << "Dohvacanje najvece vrijednosti trajalo je: " << elapsed.count() << " ms." << std::endl;

        return max_val;
    }

    std::vector<double> get_n_min_values(int n, const std::vector<double>& data) {
        auto start = std::chrono::high_resolution_clock::now();

        std::vector<double> result;
        if (!data.empty() && n > 0) {
            int count = std::min(n, static_cast<int>(data.size()));
            result.assign(data.begin(), data.begin() + count);
        }

        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double, std::milli> elapsed = end - start;
        std::cout << "Dohvacanje " << n << " najmanjih vrijednosti trajalo je: " << elapsed.count() << " ms." << std::endl;

        return result;
    }

    std::vector<double> get_n_max_values(int n, const std::vector<double>& data) {
        auto start = std::chrono::high_resolution_clock::now();

        std::vector<double> result;
        if (!data.empty() && n > 0) {
            int count = std::min(n, static_cast<int>(data.size()));
            result.assign(data.end() - count, data.end());
        }

        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double, std::milli> elapsed = end - start;
        std::cout << "Dohvacanje " << n << " najvecih vrijednosti trajalo je: " << elapsed.count() << " ms." << std::endl;

        return result;
    }
// Pretraživanje zapisa po vrijednosti
    std::vector<std::pair<std::string, double>> search_by_value(double value) {
        auto start = std::chrono::high_resolution_clock::now();
        std::vector<std::pair<std::string, double>> results;

        for (size_t i = 0; i < stator_winding.size(); ++i) {
            if (stator_winding[i] == value) {
                results.push_back({"stator_winding", stator_winding[i]});
            }
            if (u_d[i] == value) {
                results.push_back({"u_d", u_d[i]});
            }
            if (pm[i] == value) {
                results.push_back({"pm", pm[i]});
            }
        }

        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double, std::milli> elapsed = end - start;
        std::cout << "Pretrazivanje zapisa trajalo je: " << elapsed.count() << " ms." << std::endl;

        return results;
    }

    // Brisanje zapisa po vrijednosti
    void remove_by_value(double value) {
        auto start = std::chrono::high_resolution_clock::now();

        // Lambda funkcija za provjeru jednakosti s vrijednošću
        auto predicate = [value](double element) { return element == value; };

        // Uklanjanje zapisa koji odgovaraju vrijednosti u svakom vektoru
        stator_winding.erase(std::remove_if(stator_winding.begin(), stator_winding.end(), predicate), stator_winding.end());
        u_d.erase(std::remove_if(u_d.begin(), u_d.end(), predicate), u_d.end());
        pm.erase(std::remove_if(pm.begin(), pm.end(), predicate), pm.end());

        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double, std::milli> elapsed = end - start;
        std::cout << "Brisanje zapisa po vrijednosti " << value << " trajalo je: " << elapsed.count() << " ms." << std::endl;
    }

    // Dodavanje zapisa
    void add_record(double stator, double ud, double p_m) {
        auto start = std::chrono::high_resolution_clock::now();

        stator_winding.push_back(stator);
        u_d.push_back(ud);
        pm.push_back(p_m);

        sort_vector(stator_winding);
        sort_vector(u_d);
        sort_vector(pm);

        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double, std::milli> elapsed = end - start;
        std::cout << "Dodavanje zapisa trajalo je: " << elapsed.count() << " ms." << std::endl;
    }

    const std::vector<double>& get_stator_winding() const { return stator_winding; }
    const std::vector<double>& get_u_d() const { return u_d; }
    const std::vector<double>& get_pm() const { return pm; }
};
int main() {

    measures handler("measures_v2.csv");


    // Dohvacanje i ispisivanje najmanje i najvece vrijednosti u stupcu stator
    std::cout << "Najmanja vrijednost u stupcu stator: " << handler.get_min_value(handler.get_stator_winding()) << std::endl;
    std::cout << "Najveca vrijednost u stupcu stator: " << handler.get_max_value(handler.get_stator_winding()) << std::endl;

    // Dohvacanje i ispisivanje najmanje i najvece vrijednosti u stupcu u_d
    std::cout << "Najmanja vrijednost u stupcu u_d: " << handler.get_min_value(handler.get_u_d()) << std::endl;
    std::cout << "Najveca vrijednost u stupcu u_d: " << handler.get_max_value(handler.get_u_d()) << std::endl;

    // Dohvacanje i ispisivanje najmanje i najvece vrijednosti u stupcu pm
    std::cout << "Najmanja vrijednost u stupcu pm: " << handler.get_min_value(handler.get_pm()) << std::endl;
    std::cout << "Najveca vrijednost u stupcu pm: " << handler.get_max_value(handler.get_pm()) << std::endl;

    // Dohvacanje i ispisivanje top n najmanjih i najvecih vrijednosti u stupcu stator
    std::cout << "Top 3 najmanje vrijednosti u stupcu stator: ";
    for (auto val : handler.get_n_min_values(3, handler.get_stator_winding())) std::cout << val << " ";
    std::cout << std::endl;

    std::cout << "Top 3 najvece vrijednosti u stupcu stator: ";
    for (auto val : handler.get_n_max_values(3, handler.get_stator_winding())) std::cout << val << " ";
    std::cout << std::endl;

    // Dohvacanje i ispisivanje top n najmanjih i najvecih vrijednosti u stupcu u_d
    std::cout << "Top 3 najmanje vrijednosti u stupcu u_d: ";
    for (auto val : handler.get_n_min_values(3, handler.get_u_d())) std::cout << val << " ";
    std::cout << std::endl;

    std::cout << "Top 3 najvece vrijednosti u stupcu u_d: ";
    for (auto val : handler.get_n_max_values(3, handler.get_u_d())) std::cout << val << " ";
    std::cout << std::endl;

    // Dohvacanje i ispisivanje top n najmanjih i najvecih vrijednosti u stupcu pm
    std::cout << "Top 3 najmanje vrijednosti u stupcu pm: ";
    for (auto val : handler.get_n_min_values(3, handler.get_pm())) std::cout << val << " ";
    std::cout << std::endl;

    std::cout << "Top 3 najvece vrijednosti u stupcu pm: ";
    for (auto val : handler.get_n_max_values(3, handler.get_pm())) std::cout << val << " ";
    std::cout << std::endl;


    // Pretraživanje zapisa po određenoj vrijednosti
    double search_value = 19.10789680480957;
    auto start_search_value = std::chrono::high_resolution_clock::now();
    auto result_pairs = handler.search_by_value(search_value); // Pohranjuje parove imena stupaca i vrijednosti
    auto end_search_value = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> elapsed_search_value = end_search_value - start_search_value;
    if (!result_pairs.empty()) {
        std::cout << "Pronadeni stupci sa vrijednoscu " << search_value << ": " << std::endl;
        for (const auto& pair : result_pairs) {
            std::cout << pair.first << ": " << pair.second << std::endl;
        }
    } else {
        std::cout << "Nije pronadena ta vrijednost." << std::endl;
    }
    std::cout << "Vrijeme potrebno za trazenje vrijednosti: " << elapsed_search_value.count() << " ms." << std::endl;



    // Ovdje dodajemo novi zapis
    std::cout << "Dodajemo zapise..." << std::endl;
    handler.add_record(10.5, 20.5, 30.5);
    handler.add_record(11.5, 21.5, 31.5);
    handler.add_record(12.5, 22.5, 32.5);
    std::cout << "Zapisi su dodani." << std::endl;



    // Brišemo sve zapise
    std::cout << "Uklanjamo zapise..." << std::endl;
    handler.remove_by_value(10.5);
    handler.remove_by_value(21.5);
    handler.remove_by_value(32.5);
    std::cout << "Zapisi su uklonjeni." << std::endl;


    // Provjeravamo je li zapis uklonjen
    std::vector<double> values_to_check = {10.5, 21.5, 32.5};
    bool all_removed = true;

    for (double value : values_to_check) {
        auto result = handler.search_by_value(value);
        if (!result.empty()) {
            all_removed = false;
            std::cout << "Nisu svi zapisi uklonjeni. Pronađeni zapisi s vrijednoscu " << value << ":" << std::endl;
            for (const auto& pair : result) {
                std::cout << pair.first << ": " << pair.second << std::endl;
            }
        }
    }

    if (all_removed) {
        std::cout << "Svi trazeni zapisi su uklonjeni." << std::endl;
    }
    return 0;
}

