Opis projekta

Zadatak:
1. Pretraživanje zapisa po ključu ili vrijednosti, pretraživanje n zapisa po
ključu ili vrijednosti
2. Brisanje zapisa po ključu ili vrijednosti, brisanje n zapisa po ključu ili
vrijednosti
3. Dohvaćanje najvećih i najmanjih vrijednosti, dohvaćanje n najvećih i
najmanjih zapisa po vrijednosti
4. Dodavanje zapisa po ključu ili vrijednosti, dodavanje n zapisa po ključu
ili vrijednosti.

Stupci koji su odabrani su: stator_winding, u_d i pm.

Koristene funkcije:

-Konstruktor: measures(const std::string& file_path): Učitava podatke iz datoteke i sortira ih.
Sortiranje: void sort_vector(std::vector<double>& vec): Sortira vektor brojcanih vrijednosti.

Dohvacanje vrijednosti:
double get_min_value(const std::vector<double>& data): Dohvaća minimalnu vrijednost iz sortiranog vektora.
double get_max_value(const std::vector<double>& data): Dohvaća maksimalnu vrijednost iz sortiranog vektora.
std::vector<double> get_n_min_values(int n, const std::vector<double>& data): Dohvaća n najmanjih vrijednosti iz sortiranog vektora.
std::vector<double> get_n_max_values(int n, const std::vector<double>& data): Dohvaća n najvećih vrijednosti iz sortiranog vektora.

Pretrazivanje i brisanje:
std::vector<std::pair<std::string, double>> search_by_value(double value): Pretrazuje sve vektore za odredenu vrijednost i vraca parove stupac-vrijednost gdje je vrijednost pronadena.
void remove_by_value(double value): Uklanja sve instance određene vrijednosti iz svih vektora.

Dodavanje zapisa:
void add_record(double stator, double ud, double p_m): Dodaje novi zapis u svaki vektor i ponovno ih sortira.

Getter metode:
const std::vector<double>& get_stator_winding() const: Pruza pristup vektoru stator_winding.
const std::vector<double>& get_u_d() const: Pruza pristup vektoru u_d.
const std::vector<double>& get_pm() const: Pruza pristup vektoru pm.


Koristene strukture podataka:

-std::vector<double>: za pohranu i manipulaciju numeričkim podacima.

-std::pair<std::string, double> za pohranu rezultata pretrazivanja po vrijednosti.

-std::chrono::high_resolution_clock, std::chrono::duration: Koristen za mjerenje vremena izvrsavanja određenih operacija u milisekundama. 



Dobiveni rezultati:

Najmanja vrijednost u stupcu stator: Dohvacanje najmanje vrijednosti trajalo je: 0.001 ms.
-25.2909

Najveca vrijednost u stupcu stator: Dohvacanje najvece vrijednosti trajalo je: 0.0004 ms.
131.879

Najmanja vrijednost u stupcu u_d: Dohvacanje najmanje vrijednosti trajalo je: 0.0004 ms.
13.7619

Najveca vrijednost u stupcu u_d: Dohvacanje najvece vrijednosti trajalo je: 0.0004 ms.
67.2716

Najmanja vrijednost u stupcu pm: Dohvacanje najmanje vrijednosti trajalo je: 0.0004 ms.
18.5858

Najveca vrijednost u stupcu pm: Dohvacanje najvece vrijednosti trajalo je: 0.0004 ms.
141.363

Top 3 najmanje vrijednosti u stupcu stator: Dohvacanje 3 najmanjih vrijednosti trajalo je: 0.003 ms.
-25.2909 -25.2351 -25.1629

Top 3 najvece vrijednosti u stupcu stator: Dohvacanje 3 najvecih vrijednosti trajalo je: 0.0013 ms.
131.869 131.876 131.879

Top 3 najmanje vrijednosti u stupcu u_d: Dohvacanje 3 najmanjih vrijednosti trajalo je: 0.0013 ms.
13.7619 14.9828 15.5175

Top 3 najvece vrijednosti u stupcu u_d: Dohvacanje 3 najvecih vrijednosti trajalo je: 0.0018 ms.
67.2631 67.266 67.2716

Top 3 najmanje vrijednosti u stupcu pm: Dohvacanje 3 najmanjih vrijednosti trajalo je: 0.0012 ms.
18.5858 18.5869 18.5873

Top 3 najvece vrijednosti u stupcu pm: Dohvacanje 3 najvecih vrijednosti trajalo je: 0.0017 ms.
141.29 141.311 141.363

Pretrazivanje zapisa trajalo je: 3.0166 ms.

Pronadeni stupci sa vrijednoscu 19.1079:
pm: 19.1079

Vrijeme potrebno za trazenje vrijednosti: 3.6209 ms.

Dodajemo zapise...

Dodavanje zapisa trajalo je: 576.714 ms.
Dodavanje zapisa trajalo je: 562.866 ms.
Dodavanje zapisa trajalo je: 550.862 ms.

Zapisi su dodani.

Uklanjamo zapise...

Brisanje zapisa po vrijednosti 10.5 trajalo je: 10.0744 ms.
Brisanje zapisa po vrijednosti 21.5 trajalo je: 10.7368 ms.
Brisanje zapisa po vrijednosti 32.5 trajalo je: 9.9251 ms.

Zapisi su uklonjeni.

Pretrazivanje zapisa trajalo je: 2.9162 ms.
Pretrazivanje zapisa trajalo je: 2.9202 ms.
Pretrazivanje zapisa trajalo je: 2.9128 ms.

Svi trazeni zapisi su uklonjeni.

